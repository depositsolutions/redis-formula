{%- from "redis/map.jinja" import sentinel with context %}

include:
  {# We want to deploy the config files first to prevent the default distro
  sentinel config from kicking in #}
  - .config
  - .install
  - .service
