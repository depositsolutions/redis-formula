include:
  - .config

{%- from "redis/map.jinja" import redis with context %}

{##  In CentOS come redis server and sentinel together in one package.
In Debian the sentinel is to be installed as own package.  ##}


{% if grains['os'] == 'Debian' %}
{% if redis.sentinel.enabled %}
redis sentinel packages:
  pkg.installed:
  - name: {{ redis.sentinel.package }}
  - require:
    - user: redis
    - group: redis
    - file: {{ redis.sentinel.service.confile }}
{% endif %}
{% endif %}
