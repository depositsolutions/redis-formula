{%- from "redis/map.jinja" import redis with context %}

ensure redis group for sentinel:
  group.present:
    - name: redis
    - system: True

ensure redis user for sentinel:
  user.present:
    - name: redis
    - fullname: Redis system user
    - system: True
    - home: /var/lib/redis
    - shell: /bin/false
    - gid_from_name: redis
    - require:
      - group: redis

{#- Deploy initial version of sentinel file if not present. Sentinel config file
is not deployed by default. #}
deploy sentinel config:
  file.managed:
    - name: {{ redis.sentinel.service.confile }}
    - source: salt://redis/sentinel/files/sentinel.conf
    - template: jinja
    - user: redis
    - group: redis
    - mode: 640
    - makedirs: True
    - context:
        new_deployment: True
    - onlyif: "! test -f {{ redis.sentinel.service.confile }}"
    - require:
      - user: redis

{# If the sentinel file is already deployed the config management system must make
sure to not rewrite the cluster state part of the config file #}
update sentinel config:
  file.blockreplace:
    - name: {{ redis.sentinel.service.confile }}
    - marker_start: "### Managed by Salt - BEGIN ###"
    - marker_end: "### Managed by Salt - END ###"
    - sources:
      - salt://redis/sentinel/files/sentinel.conf
    - onlyif: "grep Salt {{ redis.sentinel.service.confile }} 2>&1 1>/dev/null"
    - require:
      - user: redis
