{%- from "redis/map.jinja" import redis with context %}

include:
  - .config

deploy systemd file:
  file.managed:
  - name: /lib/systemd/system/redis-sentinel.service
  - source: salt://redis/sentinel/files/{{ grains['os'] | lower }}_systemd.service
  - template: jinja
  - user: root
  - group: root
  - mode: 640
  module.run:
    - name: service.systemctl_reload
    - onchanges:
      - file: deploy systemd file

redis_sentinel:
  service.running:
  - enable: True
  - restart: True
  - name: {{ redis.sentinel.service.name }}
  - watch:
    - file: {{ redis.sentinel.service.confile }}
    - file: deploy systemd file
