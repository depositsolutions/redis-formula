{%- from "redis/map.jinja" import redis with context %}

{% set cluster = salt['pillar.get']('redis:server:cluster:enabled') %}
{% if cluster = True %}
include:
  - redis.addon.teststate
{% endif %}
gg
