{%- from "redis/map.jinja" import redis with context %}

create the systemd environment file:
  file.managed:
    - name: /etc/default/redisexporter
    - mode: 644
    - contents: |
        REDIS_ADDR={{ grains['ip_interfaces']['eth0'][0] }}:{{ redis.cluster.master.port }}
        REDIS_PASSWORD={{ redis.cluster.master.masterauth }}

shipping the systemd startupfile for redis_exporter:
  file.managed:
    - name: /lib/systemd/system/redis_exporter.service
    - source: salt://redis/addon/redisexporter/files/redis_exporter.service
    - makedirs: true
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - require:
      - file: /etc/default/redisexporter
  cmd.run:
    - name: 'systemctl daemon-reload'
    - onchanges:
      - file: /lib/systemd/system/redis_exporter.service

{{ redis.exporter.service.user }}:
  group:
    - present
    - system: True
  user:
    - present
    - name: {{ redis.exporter.service.user }}
    - shell: /bin/false
    - fullname: redis_exporter system user
    - home: /home/redis_exporter
    - system: True
    - groups:
      - {{ redis.exporter.service.group }}
    - require:
      - group: {{ redis.exporter.service.group }}
  service.running:
    - enable: True
    - name: {{ redis.exporter.service.name }}
    - require:
      - user: {{ redis.exporter.service.user }}
