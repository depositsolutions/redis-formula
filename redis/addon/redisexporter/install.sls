{%- from "redis/map.jinja" import redis with context %}


shipping the redis_exporter binary:
  file.managed:
  {% if grains['os'] == 'Debian' %}
  - name: {{ redis.exporter.service.bin_dir }}/redis_exporter
  {% endif %}
  {% if grains['os'] == 'CentOS' %}
  - name: {{ redis.exporter.service.bin_dir }}/redis_exporter
  {% endif %}
  - source: salt://redis/addon/redisexporter/binary/redis_exporter
  - makedirs: True
  - user: root
  - group: root
  - mode: 755
  - backup: minion
