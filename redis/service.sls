{%- from "redis/map.jinja" import redis with context %}

include:
  - .config

redis_service:
  service.running:
  - enable: True
  - restart: True
  - name: {{ redis.server.service }}
  - watch:
    {% if grains['os'] == 'Debian' %}
    - file: {{ redis.server.confile }}
    {% endif %}
    {% if grains['os'] == 'CentOS' %}
    - file: /etc/redis.conf
    {% endif %}
  - require:
    - user: {{ redis.server.service_user }}
