{%- from "redis/map.jinja" import redis with context %}

include:
  - .config

redis-server:
  pkg.installed:
    - name: {{ redis.package }}
    - require:
      - user: redis
      - group: redis
      - file: {{ redis.server.confile }}
